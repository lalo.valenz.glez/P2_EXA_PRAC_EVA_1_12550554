package com.mevg.lalo.exa_prac_eva_1_12550554;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

public class MainActivity extends AppCompatActivity {
    Button btnCopia, btnContenido;
    EditText etContenido, etNombre;
    RadioButton rbInternal, rbExternal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnCopia = (Button) findViewById(R.id.copia);
        btnContenido = (Button) findViewById(R.id.contenido);
        etContenido = (EditText) findViewById(R.id.archivo);
        etNombre = (EditText) findViewById(R.id.nombre);
        rbExternal = (RadioButton) findViewById(R.id.external);
        rbInternal = (RadioButton) findViewById(R.id.internal);
        crearArchivo();

    }

    private void crearArchivo(){
        try {
            OutputStream os = openFileOutput("file.txt", MODE_APPEND);
            OutputStreamWriter osw = new OutputStreamWriter(os);
            BufferedWriter writer = new BufferedWriter(osw);
            String cadena = "btnCopia = (Button) findViewById(R.id.copia);\n" +
                            "btnContenido = (Button) findViewById(R.id.contenido);\n" +
                            "etContenido = (EditText) findViewById(R.id.archivo);\n" +
                            "etNombre = (EditText) findViewById(R.id.nombre);\n" +
                            "rbExternal = (RadioButton) findViewById(R.id.external);\n" +
                            "rbInternal = (RadioButton) findViewById(R.id.internal);";
            writer.write(cadena);
            writer.close();
            os.close();
            Toast.makeText(MainActivity.this, "Archivo creado!", Toast.LENGTH_SHORT).show();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void cargarArchivo(){
        //InputStream  is = openFil
    }

}
